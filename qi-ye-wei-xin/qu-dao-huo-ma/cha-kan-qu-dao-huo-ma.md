---
description: 查看已创建的员工活码
---

# 查看员工活码

![](../../.gitbook/assets/qrcode1.jpg)

员工活码模块支持展示活码分组、全部活码列表及相关信息列。

左侧为全部活码及我创建的活码，支持分组搜索、按条件升序降序筛选，支持新建分组。

右侧为全部活码列表，显示活码名称、添加次数、使用的员工、更新者账号、更新时间、创建者账号及创建时间。

#### 操作列支持编辑、统计、下载和删除操作：

**编辑活码：**除活码名称外，其他设置均可修改编辑。

**统计活码：**提供今日添加联系人数和总添加联系人数看板。

![](../../.gitbook/assets/qrcode2.jpg)

支持按日期统计和按员工统计两种方式。点击某个日期/员工，进入统计详情页。

统计数量为0时，页面显示“当前没有任何数据”。

统计数量有值时，显示联系人列表，包括联系人企业微信名称，所属员工及操作按钮。

#### 操作按钮包括联系人详情和会话存档：

**联系人详情：**点击直接跳转至联系人详情页面，显示该联系人的状态，除Dmartech原有联系人详情信息外，新增联系人企业微信卡片。

![](<../../.gitbook/assets/14 (2).png>)

**数据导出：**统计页面右上角点击“下载”按钮，在弹窗中选择导出文件格式、编辑邮件主题及接收的邮箱地址即可。

![下载数据页面](<../../.gitbook/assets/16 (1).png>)

以上即为【员工活码】功能的全部介绍。

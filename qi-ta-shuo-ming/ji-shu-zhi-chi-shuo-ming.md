# 技术支持说明

Dmartech派工单使用说明如下：

点此查看中文说明

### **1.Login** <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-1.login" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-1.login"></a>

**https://webpower.kf5.com/request/guest/**

**when you  use the ticket system for the first time, you need to click "**_**sign up to submit and track your requests**_**" at the bottom of the page to register your account with email**

![](<../.gitbook/assets/image (450).png>)

![](<../.gitbook/assets/image (451).png>)

**Click--->"change **_**personal information 、password**_**" at the top right corner**&#x20;

![](<../.gitbook/assets/image (452).png>)

### **2.Homepage** <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-2.homepage" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-2.homepage"></a>

**"**_**Homepage**_**" summarizes the frequently fed back operation problems, and can search the keywords in the search box.**

![](<../.gitbook/assets/image (453).png>)

### **3.submit a new request** <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-3.submitanewrequest" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-3.submitanewrequest"></a>

**Click "**_**submit a new request**_**" → fill in the submit related to request, choose  problems type, ticket requester and the detailed description of the problem in turn (attachments, pictures and other information can be added to the detailed description)**

![](<../.gitbook/assets/image (454).png>)

### **4.check your existing requests** <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-4.checkyourexistingrequests" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-4.checkyourexistingrequests"></a>

**Click "check your existing requests" -→ you can view the list of submitted requests and the details of submitted requests**

![](<../.gitbook/assets/image (458).png>)

**Click "view requests" → check the ticket status, assigned time, technical reply and other relevant information, and click "push" to urge the technology to reply the request quickly**

![](<../.gitbook/assets/image (459).png>)

### **** <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-1.-deng-lu" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-1.-deng-lu"></a>

### **** <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-1.-deng-lu" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-1.-deng-lu"></a>

### **** <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-1.-deng-lu" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-1.-deng-lu"></a>

### **1.登录**  <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-1.-deng-lu" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-1.-deng-lu"></a>

#### &#x20;**** [**https://webpower.kf5.com/request/guest/**](https://webpower.kf5.com/request/guest/) <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-httpswebpower.kf5.comrequestguest" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-httpswebpower.kf5.comrequestguest"></a>

**首次使用派工单系统，需要点击页面下方"**_**请先注册以提交和追踪问题**_** "用邮箱注册账号**

![](<../.gitbook/assets/image (460).png>)

![](<../.gitbook/assets/image (461).png>)

**点击右上角-->可以修改个人信息、修改密码**

![](<../.gitbook/assets/image (462).png>)

### **2.首页** <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-2.-shou-ye" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-2.-shou-ye"></a>

**"**_**首页**_**"汇总经常反馈的操作问题，可以对关键词在搜索框进行搜索。**

![](<../.gitbook/assets/image (463).png>)

### **3.提交新工单** <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-3.-ti-jiao-xin-gong-dan" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-3.-ti-jiao-xin-gong-dan"></a>

**点击"**_**提交新工单**_**"→依次填写工单相关的标题、选择问题类型、工单来源以及问题的详细描述 （详细描述中可以添加附件、图片等信息）**

![](<../.gitbook/assets/image (464).png>)

### **4.查看工单处理** <a href="#webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-4.-cha-kan-gong-dan-chu-li" id="webpower-gong-dan-xi-tong-shi-yong-zhi-nan-for-ke-hu-can-kao-4.-cha-kan-gong-dan-chu-li"></a>

**点击"**_**查看工单处理**_**"-→可以查看已经提交工单的列表以及已提交工单的详细情况**

![](<../.gitbook/assets/image (465).png>)

**在"**_**查看工单**_**" → 查看工单受理状态、受理的时间、技术相关的回复等相关信息、以及可以点击催一下，催促技术快速回复该工单**

![](<../.gitbook/assets/image (466).png>)

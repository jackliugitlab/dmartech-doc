# 联系人标签列表/contactLabelList





{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/contactLabelList" method="get" summary=" 联系人标签列表" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="path" name="name" type="string" %}
查询主键子段名
{% endswagger-parameter %}

{% swagger-parameter in="path" name="value" type="string" %}
查询值
{% endswagger-parameter %}

{% swagger-parameter in="path" name="customerId" type="string" %}
联系人customerId（联系人分页列表接口提供） 
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description="" %}
```
{
    "code": 0,
    "message": "Successful.",
    "traceNumber": "c1e89108db6e4f7e8e79cc902a7a2386",
    "data": {
        "customerLabelList": [
            {
                "name": "哈哈哈的标签",
                "sn": "0f8e94d9965e8c67"
            },
            {
                "name": "9-10-标签不属于条件测试",
                "sn": "7873ad641d292e48"
            }
        ],
        "fansLabelList": [
            {
                "appid": "wxef09590e7de97fd0",
                "fansLabelInfoList": []
            },
            {
                "appid": "wx7a137c6c7eaa3f70",
                "fansLabelInfoList": [
                    {
                        "name": "1029034",
                        "sn": "1dfae073b2a4b46a"
                    },
                    {
                        "name": "再次更改09-5",
                        "sn": "fedd1f9af403845b"
                    }
                ]
            }
        ]
    }
}
```
{% endswagger-response %}

{% swagger-response status="403" description="" %}
```
{
    "code": 40003,
    "message": "Forbidden",
    "traceNumber": "dcfd0c46b9e64d5a8a20b852388f6310",
    "data": null
}
```
{% endswagger-response %}
{% endswagger %}

### 注意事项

name  value为一组查询条件，customerI为一组查询条件，两者不能同时存在。

### 返回参数说明 <a href="fan-hui-can-shu-shuo-ming" id="fan-hui-can-shu-shuo-ming"></a>

| 字段名               | 描述       |
| ----------------- | -------- |
| customerLabelList | 联系人标签列表  |
| fansLabelList     | 微信粉丝标签列表 |
| appid             | 公众号id    |

# 联系人属性查询/contactCustomerDetails



{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/contactCustomerDetails" method="get" summary=" 联系人客户属性" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="path" name="name" type="string" %}
查询主键子段名
{% endswagger-parameter %}

{% swagger-parameter in="path" name="value" type="string" %}
查询值
{% endswagger-parameter %}

{% swagger-parameter in="path" name="customerId" type="string" %}
联系人customerId（联系人分页列表接口提供） 
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description="" %}
```
{
    "code": 0,
    "message": "Successful.",
    "traceNumber": "345eddf50ecf45c79a3ef328061513db",
    "data": {
        "resultList": [

            {
                "fieldName": "health_dbp",
                "value": "",
                "name": "舒张压"
            },
            {
                "fieldName": "health_sbp",
                "value": "",
                "name": "收缩压"
            },
            {
                "fieldName": "health_age",
                "value": "",
                "name": "体检年龄"
            }
             ]
    }
}

```
{% endswagger-response %}

{% swagger-response status="403" description="" %}
```
{
    "code": 40003,
    "message": "Forbidden",
    "traceNumber": "dcfd0c46b9e64d5a8a20b852388f6310",
    "data": null
}



```
{% endswagger-response %}
{% endswagger %}

### 注意事项

name  value为一组查询条件，customerI为一组查询条件，两者不能同时存在。

### 返回参数说明 <a href="fan-hui-can-shu-shuo-ming" id="fan-hui-can-shu-shuo-ming"></a>

| 字段名       | 描述     |
| --------- | ------ |
| fieldName | 联系人字段名 |
| value     | 属性值    |
| name      | 字段中文名  |

# 标签下联系人列表/contactByLabelPageList



{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/contactByLabelPageList" method="get" summary=" 标签下联系人列表" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="path" name="sn" type="string" %}
标签sn (联系人标签列表接口提供)
{% endswagger-parameter %}

{% swagger-parameter in="path" name="pageNo" type="number" %}
页码
{% endswagger-parameter %}

{% swagger-parameter in="path" name="pageSize" type="number" %}
分页数量
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description=" 成功事例" %}
```
{
    "code": 0,
    "message": "Successful.",
    "traceNumber": "7aceb14f2e944ff1a5d5ceb35781ea40",
    "data": {
        "totalPage": 1447,
        "totalRow": 14468,
        "resultList": [
            {
                "sex_WeChat": null,
                "ageage": null,
                "邮箱": "******",
                "country": null,
                "nickname_WeChat": null,
                "customerId": "fffa2f8a-30c1-4153-875d-4ab9d79666f5",
                "微信": "******",
                "name": null,
                "mobile": null,
                "导入时间": "******",
                "更新时间": "******",
                "city_WeChat": null
            }
 
        ]
    }
}
```
{% endswagger-response %}

{% swagger-response status="403" description="失败案例" %}
```
{
    "code": 40003,
    "message": "Forbidden",
    "traceNumber": "dcfd0c46b9e64d5a8a20b852388f6310",
    "data": null
}
```
{% endswagger-response %}
{% endswagger %}

### 注意事项

字段是否脱敏在后台配置

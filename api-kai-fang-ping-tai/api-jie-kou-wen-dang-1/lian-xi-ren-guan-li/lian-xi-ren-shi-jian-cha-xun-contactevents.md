# 联系人事件查询/contactEvents

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/contactEvents" method="get" summary="联系人事件查询" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="query" name="customerId " type="string" %}
联系人ID，可通过/contact/contactPageList 获取
{% endswagger-parameter %}

{% swagger-parameter in="query" name="pageNo " type="string" %}
页码
{% endswagger-parameter %}

{% swagger-parameter in="query" name="pageSize " type="string" %}
每页多少条，上限20
{% endswagger-parameter %}

{% swagger-parameter in="query" name="fromDate" type="string" %}
起始时间，非必填，例如2021-09-17
{% endswagger-parameter %}

{% swagger-parameter in="query" name="toDate" type="string" %}
4结束时间，非必填，例如2021-09-30
{% endswagger-parameter %}

{% swagger-response status="200" description="Cake successfully retrieved." %}
```
{
    "code": 0,
    "message": "Successful.",
    "traceNumber": "82f18b74383e417b857112979ddd0ed1",
    "data": {
    
    }
}
```
{% endswagger-response %}

{% swagger-response status="404" description="Could not find a cake matching this query." %}
```
{    "message": "Ain't no cake like that."}
```
{% endswagger-response %}
{% endswagger %}

参数获取：**（截图url为测试环境，实际以上方信息为准。）**

customerId \* 联系人ID，可通过/contact/contactPageList 获取

![get customerId](../../../.gitbook/assets/contactpagelist.png)

![use customerId to get events](../../../.gitbook/assets/WX20211124-102811@2x.png)


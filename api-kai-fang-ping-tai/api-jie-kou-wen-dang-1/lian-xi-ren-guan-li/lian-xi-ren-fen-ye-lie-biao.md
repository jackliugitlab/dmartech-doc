# 联系人分页列表/contactPageList

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/contactPageList" method="get" summary=" 联系人分页列表" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="path" name="appIds" type="array" %}
公众号appId数组（当查询customerType为3，微信粉丝客户的时候必传。）
{% endswagger-parameter %}

{% swagger-parameter in="path" name="customerType" type="number" %}
联系人列表类型 1.实名客户 2.匿名客户 3.微信粉丝客户 4.全部客户
{% endswagger-parameter %}

{% swagger-parameter in="path" name="pageNo" type="number" %}
页码
{% endswagger-parameter %}

{% swagger-parameter in="path" name="pageSize" type="number" %}
分页数量
{% endswagger-parameter %}

{% swagger-parameter in="path" name="searchField" type="string" %}
搜索字段 （搜索字段为联系人字段详情接口的sn字段，字段能否搜索请参考联系人字段详情接口的searchType字段）
{% endswagger-parameter %}

{% swagger-parameter in="path" name="searchValue" type="string" %}
 搜索值
{% endswagger-parameter %}

{% swagger-parameter in="path" name="sortField" type="string" %}
 排序字段 联系人字段详情接口的fieldName字段，字段能否排序请参考联系人字段详情接口的sortType字段
{% endswagger-parameter %}

{% swagger-parameter in="path" name="sortOrder" type="string" %}
 排序类型 desc(倒序)/asc(正序)
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description=" 成功事例" %}
```
{
    "code": 0,
    "message": "Successful.",
    "traceNumber": "95cb7914841740e8896316702c322518",
    "data": {
        "totalPage": 86,
        "totalRow": 852,
        "resultList": [
            {
                "联系人名称": "yi",
                "手机号": "15865591190",
                "customerId": "7e2375a3-6f48-4ffe-bbc8-3352e09f194f",
                "微信": "是",
                "导入时间": "2019-12-19 13:56",
                "更新时间": "2021-04-08 06:06"
            }
        ]
    }
}
```
{% endswagger-response %}

{% swagger-response status="403" description="失败案例" %}
```
{
    "code": 40003,
    "message": "Forbidden",
    "traceNumber": "dcfd0c46b9e64d5a8a20b852388f6310",
    "data": null
}
```
{% endswagger-response %}
{% endswagger %}

### 注意事项

字段是否脱敏在后台配置

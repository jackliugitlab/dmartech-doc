# 创建标签/createLabel

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/createLabel" method="post" summary=" 创建标签" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-key" type="string" %}
 访问凭证
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
 访问签名
{% endswagger-parameter %}

{% swagger-parameter in="body" name="groupSn" type="string" %}
 分组sn
{% endswagger-parameter %}

{% swagger-parameter in="body" name="name" type="string" %}
 标签名称
{% endswagger-parameter %}

{% swagger-response status="200" description="Cake successfully retrieved." %}
```
{
  "code": 0,
  "data": {
    "id":"number"
    "name": "string",
    "sn": "string"
  },
  "message": "string",
  "traceNumber": "string"
}
```
{% endswagger-response %}

{% swagger-response status="404" description="Could not find a cake matching this query." %}
```
{    "message": "Ain't no cake like that."}
```
{% endswagger-response %}
{% endswagger %}

#### 响应参数

| 参数名  | 类型     | 描述   |
| ---- | ------ | ---- |
| sn   | string | 标签sn |
| name | string | 标签名称 |
| id   | number | 标签id |


# 标签分页列表/labelPageList

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/labelPageList" method="get" summary=" 标签分页列表" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-key" type="string" %}
 访问凭证
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
 访问签名
{% endswagger-parameter %}

{% swagger-parameter in="query" name="pageNo" type="string" %}
 页码
{% endswagger-parameter %}

{% swagger-parameter in="query" name="pageSize" type="string" %}
 分页数量
{% endswagger-parameter %}

{% swagger-parameter in="query" name="groupSn" type="string" %}
 分组sn
{% endswagger-parameter %}

{% swagger-parameter in="query" name="keywords" type="string" %}
 关键字(名称)
{% endswagger-parameter %}

{% swagger-parameter in="query" name="sortField" type="string" %}
 排序字段 createDate:创建时间,updateDate:更新时间
{% endswagger-parameter %}

{% swagger-parameter in="query" name="sortOrder" type="string" %}
 排序类型 desc:倒序,asc:正序
{% endswagger-parameter %}

{% swagger-response status="200" description="Cake successfully retrieved." %}
```
{
  "code": 0,
  "data": {
    "resultList": [
      {
        "contactCount": 0,
        "id":"number"
        "isConceal": 0,
        "labelName": "string",
        "sn": "string",
        "updateDate": "2021-01-01 00:00:00"
      }
    ],
    "totalPage": 0,
    "totalRow": 0
  },
  "message": "string",
  "traceNumber": "string"
}
```
{% endswagger-response %}

{% swagger-response status="404" description="Could not find a cake matching this query." %}
```
{    "message": "Ain't no cake like that."}
```
{% endswagger-response %}
{% endswagger %}

#### 响应参数

| 参数名          | 类型      | 描述                                   |
| ------------ | ------- | ------------------------------------ |
| sn           | string  |  标签sn                                |
| labelName    | string  |  标签名称                                |
| isConceal    | integer | <p> 隐藏状态 </p><p>0:不隐藏</p><p>1:隐藏</p> |
| contactCount | integer |  联系人数量                               |
| updateDate   | string  |  更新时间                                |
| id           | number  | 标签id                                 |

# 标签分组列表/labelGroupList

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/labelGroupList" method="get" summary=" 标签分组列表" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-key " type="string" %}
 访问凭证
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
 访问签名
{% endswagger-parameter %}

{% swagger-parameter in="query" name="sortField" type="string" %}
 排序字段 createDate:创建时间,updateDate:更新时间,name:分组名称
{% endswagger-parameter %}

{% swagger-parameter in="query" name="sortOrder" type="string" %}
 排序类型 desc:倒序,asc:正序
{% endswagger-parameter %}

{% swagger-response status="200" description="Cake successfully retrieved." %}
```
{
  "code": 0,
  "data": {
    "resultList": [
      {
        "createDate": "2021-01-01 00:00:00",
        "name": "string",
        "parentSn": "string",
        "sn": "string"
      }
    ]
  },
  "message": "string",
  "traceNumber": "string"
}
```
{% endswagger-response %}

{% swagger-response status="404" description="Could not find a cake matching this query." %}
```
{    "message": "Ain't no cake like that."}
```
{% endswagger-response %}
{% endswagger %}

#### 响应参数

| 参数名        | 类型     | 描述     |
| ---------- | ------ | ------ |
| sn         | string | 分组sn   |
| parentSn   | string | 父级分组sn |
| name       | string | 分组名称   |
| createDate | string | 创建时间   |

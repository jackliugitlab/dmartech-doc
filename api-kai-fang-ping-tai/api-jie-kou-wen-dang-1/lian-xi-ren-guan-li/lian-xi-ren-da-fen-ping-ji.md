# 联系人打分评级/contactGradeScoreList

&#x20;   &#x20;

​

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/contactGradeScoreList" method="get" summary=" 联系人打分评级" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="path" name="name" type="string" %}
查询主键子段名
{% endswagger-parameter %}

{% swagger-parameter in="path" name="value" type="string" %}
查询值
{% endswagger-parameter %}

{% swagger-parameter in="path" name="customerId" type="string" %}
联系人customerId（联系人分页列表接口提供） 
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description="" %}
```
{
    "code": 0,
    "message": "Successful.",
    "traceNumber": "ca2725f51d91467788b65b340feeb0d6",
    "data": {
      "resultList": [
        {
          "gradeRule": "string",
          "rank": "string",
          "score": "string"
        }
      ]
    }
}
```
{% endswagger-response %}

{% swagger-response status="403" description="" %}
```
{
    "code": 40003,
    "message": "Forbidden",
    "traceNumber": "dcfd0c46b9e64d5a8a20b852388f6310",
    "data": null
}
```
{% endswagger-response %}
{% endswagger %}

### 注意事项

name  value为一组查询条件，customerI为一组查询条件，两者不能同时存在。

### 返回参数说明 <a href="fan-hui-can-shu-shuo-ming" id="fan-hui-can-shu-shuo-ming"></a>

| 字段名       | 描述   |
| --------- | ---- |
| gradeRule | 评级规则 |
| score     | 实际得分 |
| rank      | 分值等级 |


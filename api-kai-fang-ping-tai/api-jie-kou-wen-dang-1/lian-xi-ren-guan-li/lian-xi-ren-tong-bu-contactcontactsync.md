# 联系人同步/contact/contactSync



{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/contactSync" method="get" summary=" 联系人同步" %}
{% swagger-description %}
用于获取Dmartech中某时间范围内新增的联系人，或按照标签查询联系人
{% endswagger-description %}

{% swagger-parameter in="path" name="customerType" type="number" required="true" %}
联系人列表类型 1.实名客户 2.匿名客户 3.微信粉丝客户 4.全部客户
{% endswagger-parameter %}

{% swagger-parameter in="path" name="skip" type="String" %}
查询游标 由上一次查询返回，不传默认从第一页开始查询
{% endswagger-parameter %}

{% swagger-parameter in="path" name="pageSize" type="integer" required="true" %}
分页数量，，不超过10000。
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-parameter in="path" name="SearchType" type="number" required="true" %}
**查询类型 1.更新时间。2.创建时间。3.按标签。如果是1或2，请指定开始、结束日期。如果是3，请指定标签sn。**
{% endswagger-parameter %}

{% swagger-parameter in="path" name="fromDate" type="String" %}
查查询日期的开始日期,例如：2021-09-01 08:00:00
{% endswagger-parameter %}

{% swagger-parameter in="path" name="toDate" type="String" %}
查询日期的结束日期,例如：2021-09-01 08:00:00
{% endswagger-parameter %}

{% swagger-parameter in="path" name="sn" type="String" %}
标签sn（通过/contact/labelPageList 获取）
{% endswagger-parameter %}

{% swagger-response status="200" description=" 成功事例" %}
```
{
    "code": 0,
    "message": "Successful.",
    "traceNumber": "0f72efe13a34422dbb102dd8d6fb1495",
    "data": {
        "totalPage": 1,
        "totalRow": 11,
        "skip": "1635930418738",
        "resultList": [
            {
                "邮箱": null,
                "任务2_实际得分": null,
                "昵称_微信": null,
                "联系人名称": null,
                "手机号": null,
                "customerId": "8371d4c9-50a5-46c9-822b-ce54bf3728ee",
                "微信": "否",
                "创建时间": null,
                "导入时间": "2021-11-01 09:28",
                "更新时间": "2021-11-23 19:20",
                "DMT智慧营销测试_openid": null,
                "性别": null
            },
            {
                "邮箱": null,
                "任务2_实际得分": null,
                "昵称_微信": "Lingard",
                "联系人名称": null,
                "手机号": null,
                "customerId": "df65d02b-444f-4cee-8e07-4a73dc32f51d",
                "微信": "否",
                "创建时间": null,
                "导入时间": "2021-11-05 10:25",
                "更新时间": "2021-11-23 19:19",
                "DMT智慧营销测试_openid": null,
                "性别": null
            },
            {
                "邮箱": null,
                "任务2_实际得分": null,
                "昵称_微信": "沈十安",
                "联系人名称": null,
                "手机号": null,
                "customerId": "e3c5ecfe-5384-42c6-83ff-f1c90557eebd",
                "微信": "否",
                "创建时间": null,
                "导入时间": "2021-11-05 15:08",
                "更新时间": "2021-11-23 19:19",
                "DMT智慧营销测试_openid": null,
                "性别": null
            },
            {
                "邮箱": null,
                "任务2_实际得分": null,
                "昵称_微信": "丁彦雨航",
                "联系人名称": null,
                "手机号": null,
                "customerId": "d4a9de99-56e1-4254-baf9-2265d1056aad",
                "微信": "否",
                "创建时间": null,
                "导入时间": "2021-11-03 10:23",
                "更新时间": "2021-11-23 19:17",
                "DMT智慧营销测试_openid": null,
                "性别": null
            },
            {
                "邮箱": null,
                "任务2_实际得分": null,
                "昵称_微信": "富婆来啦",
                "联系人名称": null,
                "手机号": null,
                "customerId": "24da4a60-08da-42f1-bdc4-8c4ae67aa55c",
                "微信": "是",
                "创建时间": null,
                "导入时间": "2021-11-03 14:31",
                "更新时间": "2021-11-23 11:36",
                "DMT智慧营销测试_openid": "od-CE0lSmOeJNDYzrodVhPE9of-w",
                "性别": null
            },
            {
                "邮箱": null,
                "任务2_实际得分": null,
                "昵称_微信": null,
                "联系人名称": null,
                "手机号": null,
                "customerId": "d3483f21-8145-4972-8255-60451147b14b",
                "微信": "否",
                "创建时间": null,
                "导入时间": "2021-11-03 10:24",
                "更新时间": "2021-11-22 16:24",
                "DMT智慧营销测试_openid": null,
                "性别": null
            },
            {
                "邮箱": null,
                "任务2_实际得分": null,
                "昵称_微信": "Ding2um",
                "联系人名称": null,
                "手机号": null,
                "customerId": "b70ac55c-44a3-4213-b2f7-51d09097dcbd",
                "微信": "否",
                "创建时间": null,
                "导入时间": "2021-11-03 10:23",
                "更新时间": "2021-11-22 16:23",
                "DMT智慧营销测试_openid": null,
                "性别": null
            },
            {
                "邮箱": "ym1354@qdum.com",
                "任务2_实际得分": null,
                "昵称_微信": null,
                "联系人名称": "ym1354@qdum.com",
                "手机号": null,
                "customerId": "03eb9948-ff62-4ea6-bf42-e800519e1f82",
                "微信": "否",
                "创建时间": null,
                "导入时间": "2021-11-03 13:54",
                "更新时间": "2021-11-11 06:10",
                "DMT智慧营销测试_openid": null,
                "性别": null
            },
            {
                "邮箱": null,
                "任务2_实际得分": null,
                "昵称_微信": "梦屿千寻",
                "联系人名称": null,
                "手机号": null,
                "customerId": "846789d7-a8de-4901-a7a3-93b29b2c78d0",
                "微信": "是",
                "创建时间": null,
                "导入时间": "2021-11-09 14:41",
                "更新时间": "2021-11-09 14:42",
                "DMT智慧营销测试_openid": "od-CE0u1L8YYVnK81XAuR-qdB3rM",
                "性别": null
            },
            {
                "邮箱": null,
                "任务2_实际得分": null,
                "昵称_微信": "Ⅹ",
                "联系人名称": null,
                "手机号": null,
                "customerId": "5d4e198b-f481-46d8-b0eb-0c6ea286bb5e",
                "微信": "是",
                "创建时间": null,
                "导入时间": "2021-11-03 17:29",
                "更新时间": "2021-11-03 17:29",
                "DMT智慧营销测试_openid": "od-CE0uG3hWmHW2jpxKfkG2LJ4cs",
                "性别": null
            },
            {
                "邮箱": null,
                "任务2_实际得分": null,
                "昵称_微信": "我每天都好困",
                "联系人名称": null,
                "手机号": null,
                "customerId": "77020993-6d71-4c81-aff3-61eb6f0708f8",
                "微信": "是",
                "创建时间": null,
                "导入时间": "2021-11-03 14:36",
                "更新时间": "2021-11-03 17:06",
                "DMT智慧营销测试_openid": "od-CE0pPM1C5-OTP3PeDwweigvdk",
                "性别": null
            }
        ]
    }
}
```
{% endswagger-response %}

{% swagger-response status="403" description="失败案例" %}
```
{
    "code": 40003,
    "message": "Forbidden",
    "traceNumber": "dcfd0c46b9e64d5a8a20b852388f6310",
    "data": null
}
```
{% endswagger-response %}
{% endswagger %}

如果SearchType是1或2，则fromDate和toDate必须指定。

如果SearchType是3，则sn必须指定。

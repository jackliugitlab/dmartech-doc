# 联系人字段详情/contactFieldDetails



{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/contact/contactFieldDetails" method="get" summary="联系人字段详情" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description="" %}
```
{
    "code": 0,
    "message": "Successful.",
    "traceNumber": "47ac0ae36f0c4a58a4289c1252832231",
    "data": {
        "resultList": [
            {
                "fieldName": "weChat",
                "sn": "4c140f5832429eb1",
                "name": "微信",
                "searchType": false,
                "sortType": false
            },
            {
                "fieldName": "update_date",
                "sn": "390f451bcf04dcf1",
                "name": "更新时间",
                "searchType": false,
                "sortType": true
            }
                   ]
    }
}
```
{% endswagger-response %}

{% swagger-response status="403" description="" %}
```
{
    "code": 40003,
    "message": "Forbidden",
    "traceNumber": "dcfd0c46b9e64d5a8a20b852388f6310",
    "data": null
}
```
{% endswagger-response %}
{% endswagger %}

### 返回参数说明

| 字段名        | 描述                                      |
| ---------- | --------------------------------------- |
| fieldName  | 联系人字段名                                  |
| sn         | 联系人标识                                   |
| name       | 联系人字段中文名                                |
| searchType | 是否可以作为查询字段查询 true / false （联系人分页列表接口使用） |
| sortType   | 是否可以作为排序字段排序 true / false （联系人分页列表接口使用） |


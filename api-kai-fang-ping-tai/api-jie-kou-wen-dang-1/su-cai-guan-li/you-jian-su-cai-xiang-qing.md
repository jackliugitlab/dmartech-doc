# 邮件素材详情/emailMaterialDetails



{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/material/emailMaterialDetails" method="get" summary=" 邮件素材详情" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="path" name="materialSn" type="string" %}
素材sn
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description=" 成功事例" %}
```
{
  "code": 0,
  "data": {
    "createDate": "string",
    "createName": "string",
    "materialContent": "string",
    "materialName": "string",
    "previewUrl": "string",
    "sn": "string",
    "testSubject": "string",
    "updateDate": "string",
    "updateName": "string"
  },
  "message": "string",
  "traceNumber": "string"
}
```
{% endswagger-response %}

{% swagger-response status="403" description="失败案例" %}
```
{
    "code": 40003,
    "message": "Forbidden",
    "traceNumber": "dcfd0c46b9e64d5a8a20b852388f6310",
    "data": null
}
```
{% endswagger-response %}
{% endswagger %}

###

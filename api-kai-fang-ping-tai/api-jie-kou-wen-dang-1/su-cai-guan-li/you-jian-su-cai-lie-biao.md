# 邮件素材列表/emailMaterialPageList



{% swagger baseUrl="https://api.dmartech.cn" path="/open-api/v1/material/emailMaterialPageList" method="get" summary=" 邮件素材列表" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="path" name="groupSn" type="string" %}
分组sn
{% endswagger-parameter %}

{% swagger-parameter in="path" name="pageNo" type="number" %}
页码
{% endswagger-parameter %}

{% swagger-parameter in="path" name="pageSize" type="number" %}
分页数量
{% endswagger-parameter %}

{% swagger-parameter in="path" name="keywords" type="string" %}
 关键字 素材名称/创建者/更新者
{% endswagger-parameter %}

{% swagger-parameter in="path" name="sortField" type="string" %}
 排序字段 materialName/createDate/updateDate
{% endswagger-parameter %}

{% swagger-parameter in="path" name="sortOrder" type="string" %}
 排序类型 desc(倒序)/asc(正序)
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description=" 成功事例" %}
```
{
  "code": 0,
  "data": {
    "resultList": [
      {
        "groupSn": "string",
        "keywords": "string",
        "pageNo": 0,
        "pageSize": 0,
        "sortField": "string",
        "sortOrder": "string"
      }
    ],
    "totalPage": 0,
    "totalRow": 0
  },
  "message": "string",
  "traceNumber": "string"
}
```
{% endswagger-response %}

{% swagger-response status="403" description="失败案例" %}
```
{
    "code": 40003,
    "message": "Forbidden",
    "traceNumber": "dcfd0c46b9e64d5a8a20b852388f6310",
    "data": null
}
```
{% endswagger-response %}
{% endswagger %}

###


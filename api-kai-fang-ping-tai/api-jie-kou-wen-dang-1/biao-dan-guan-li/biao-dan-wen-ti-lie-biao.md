# 表单问题列表/questionList

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/form/questionList" method="get" summary=" 表单问题列表" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-key" type="string" %}
 访问凭证
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
 访问签名
{% endswagger-parameter %}

{% swagger-parameter in="query" name="formSn" type="string" %}
 表单sn
{% endswagger-parameter %}

{% swagger-response status="200" description="Cake successfully retrieved." %}
```
{
  "code": 0,
  "data": [
    {
      "field": "string",
      "fieldName": "string",
      "fieldType": 0,
      "sn": "string"
    }
  ],
  "message": "string",
  "traceNumber": "string"
}
```
{% endswagger-response %}

{% swagger-response status="404" description="Could not find a cake matching this query." %}
```
{    "message": "Ain't no cake like that."}
```
{% endswagger-response %}
{% endswagger %}

#### 响应参数

| 字段名       | 类型     | 描述                                                                    |
| --------- | ------ | --------------------------------------------------------------------- |
| field     | string | 字段                                                                    |
| fieldName | string | 字段名称                                                                  |
| fieldType | string | <p>字段类型 </p><p>0:字符串</p><p>1:整数</p><p>2:时间</p><p>3:小数</p><p>4:布尔值</p> |
| sn        | string | 字段sn                                                                  |


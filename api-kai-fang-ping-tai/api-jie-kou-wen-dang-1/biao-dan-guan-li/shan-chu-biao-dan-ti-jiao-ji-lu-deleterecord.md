# 删除表单提交记录/deleteRecord

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/form/deleteRecord" method="post" summary="Get Cakes" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
访问签名
{% endswagger-parameter %}

{% swagger-parameter in="body" name="_id" type="string" %}
表单提交记录ID，通过/form/submitRecordPageList 获取，可一次性提交多个_ids。
{% endswagger-parameter %}

{% swagger-response status="200" description="Cake successfully retrieved." %}
```
{
    "code": 0,
    "message": "Successful.",
    "traceNumber": "7920def7752a40399b607eb2827b2625",
    "data": null
}
```
{% endswagger-response %}
{% endswagger %}

请求实例：**（截图url为测试环境，实际以上方信息为准。）**

![](<../../../.gitbook/assets/图片 11.png>)

![](<../../../.gitbook/assets/图片 22.png>)

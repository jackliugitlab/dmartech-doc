# 表单分页列表/formPageList

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/form/formPageList" method="get" summary=" 表单分页列表" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-key" type="string" %}
 访问凭证
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
 访问签名
{% endswagger-parameter %}

{% swagger-parameter in="query" name="pageNo" type="string" %}
 页码
{% endswagger-parameter %}

{% swagger-parameter in="query" name="pageSize" type="string" %}
 分页数量
{% endswagger-parameter %}

{% swagger-parameter in="query" name="groupSn" type="string" %}
 分组sn
{% endswagger-parameter %}

{% swagger-parameter in="query" name="keywords" type="string" %}
 关键字(表单名称)
{% endswagger-parameter %}

{% swagger-parameter in="query" name="sortField" type="string" %}
 排序字段 createDate:创建时间,updateDate:更新时间
{% endswagger-parameter %}

{% swagger-parameter in="query" name="sortOrder" type="string" %}
 排序类型 desc:倒序,asc:正序
{% endswagger-parameter %}

{% swagger-parameter in="query" name="type" type="string" %}
 表单类型 JSJ:金数据表单,DMT:标准表单
{% endswagger-parameter %}

{% swagger-response status="200" description="Cake successfully retrieved." %}
```
{
  "code": 0,
  "data": {
    "resultList": [
      {
        "commitDataCount": 0,
        "commitPersonCount": 0,
        "createDate": "2021-01-01 00:00:00",
        "createName": "string",
        "formName": "string",
        "sn": "string",
        "status": "已发布",
        "type": "DMT",
        "updateDate": "2021-01-01 00:00:00",
        "updateName": "string"
      }
    ],
    "totalPage": 0,
    "totalRow": 0
  },
  "message": "string",
  "traceNumber": "string"
}
```
{% endswagger-response %}

{% swagger-response status="404" description="Could not find a cake matching this query." %}
```
{    "message": "Ain't no cake like that."}
```
{% endswagger-response %}
{% endswagger %}

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/form/groupList" method="get" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description="" %}
```
```
{% endswagger-response %}
{% endswagger %}

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/form/groupList" method="get" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description="" %}
```
```
{% endswagger-response %}
{% endswagger %}

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/form/groupList" method="get" summary="" %}
{% swagger-description %}

{% endswagger-description %}

{% swagger-parameter in="header" name="access-key" type="string" %}
访问凭证
{% endswagger-parameter %}

{% swagger-response status="200" description="" %}
```
```
{% endswagger-response %}
{% endswagger %}

#### 响应参数

| 字段名               | 类型      | 描述                                            |
| ----------------- | ------- | --------------------------------------------- |
| sn                | string  | 表单sn                                          |
| formName          | string  | 表单名称                                          |
| type              | string  | <p>表单类型 </p><p>JSJ:金数据表单</p><p>DMT:标准表单</p>   |
| status            | string  | <p>金数据表单状态:未测试/已测试/已发布</p><p>标准表单状态:正常/暂停</p> |
| commitDataCount   | integer | 提交数据量                                         |
| commitPersonCount | integer | 提交人数                                          |
| createName        | string  | 创建者                                           |
| createDate        | string  | 创建时间                                          |
| updateName        | string  | 更新者                                           |
| updateDate        | string  | 更新时间                                          |


# 获取表单提交记录/submitRecordPage

{% swagger baseUrl="https://api.dmartech.cn/openapi" path="/open-api/v1/form/submitRecordPageList" method="post" summary=" 表单提交记录分页列表" %}
{% swagger-description %}
 
{% endswagger-description %}

{% swagger-parameter in="header" name="access-key" type="string" %}
 访问凭证
{% endswagger-parameter %}

{% swagger-parameter in="header" name="access-sign" type="string" %}
 访问签名
{% endswagger-parameter %}

{% swagger-parameter in="body" name="formSn" type="string" %}
 表单sn
{% endswagger-parameter %}

{% swagger-parameter in="body" name="pageNo" type="string" %}
 页码
{% endswagger-parameter %}

{% swagger-parameter in="body" name="pageSize" type="string" %}
 分页数量
{% endswagger-parameter %}

{% swagger-parameter in="body" name="filterRelation" type="integer" %}
 过滤关系 1:且 2:或 不传默认为1
{% endswagger-parameter %}

{% swagger-parameter in="body" name="filters" type="array" %}
 过滤器
{% endswagger-parameter %}

{% swagger-parameter in="body" name="filterType" type="number" %}
 过滤器类型  

\


表单问题为字符串类型时(1:等于,2:不等于,3:包含,4:不包含,5:有值,6:没值,7:属于,8:不属于) 

\


 表单问题为时间类型时(9:绝对时间,10:相对当前时间点,11:有值,12:没值）
{% endswagger-parameter %}

{% swagger-parameter in="body" name="filterValue" type="object" %}
 过滤器值
{% endswagger-parameter %}

{% swagger-parameter in="body" name="formQuestionSn" type="string" %}
 表单问题sn ，目前支持字符串fieldType为0的表单问题
{% endswagger-parameter %}

{% swagger-response status="200" description="Cake successfully retrieved." %}
```
{
  "code": 0,
  "data": {
    "resultList": [
      {}
    ],
    "totalPage": 0,
    "totalRow": 0
  },
  "message": "string",
  "traceNumber": "string"
}
```
{% endswagger-response %}

{% swagger-response status="404" description="Could not find a cake matching this query." %}
```
{    "message": "Ain't no cake like that."}
```
{% endswagger-response %}
{% endswagger %}

> formQuestionSn字段从表单问题列表接口响应参数的字段sn获取

#### 响应参数

&#x20;    请参考表单问题列表返回参数含义

#### 过滤器请求示例

* 等于 filterType = 1&#x20;

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":1,
            "filterValue":"test@email.com"
        }
    ]
}
```

* 不等于 filterType = 2&#x20;

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":2,
            "filterValue":"test@email.com"
        }
    ]
}
```

* 包含 filterType = 3

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":3,
            "filterValue":"test@email.com"
        }
    ]
}
```

* &#x20;不包含 filterType = 4

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":4,
            "filterValue":"test@email.com"
        }
    ]
}
```

* 有值 filterType = 5

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":5
        }
    ]
}
```

* 没值 filterType = 6&#x20;

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":6
        }
    ]
}
```

* 属于 filterType = 7&#x20;

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":7,
            "filterValue":[
                "testone@email.com",
                "testtwo@email.com"
            ]
        }
    ]
}
```

* 不属于 filterType = 8&#x20;

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":8,
            "filterValue":[
                "testone@email.com",
                "testtwo@email.com"
            ]
        }
    ]
}
```

* 绝对时间 filterType = 9&#x20;

> startTime:开始时间，endTime:结束时间，时间格式为yyyy-MM-dd HH:mm

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":9,
            "filterValue":{
                "startTime":"2021-05-20 00:00",
                "endTime":"2021-05-20 23:59"
            }
        }
    ]
}
```

* 相对当前时间点 filterType = 10&#x20;

> days:几天，type:0(以内)/1(以前)

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":10,
            "filterValue":{
                "days":1,
                "type":1
            }
        }
    ]
}
```

* 有值 filterType = 11&#x20;

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":11
        }
    ]
}
```

* 没值 filterType = 12&#x20;

```
{
    "formSn":"06ef766448e0a1eb",
    "pageNo":1,
    "pageSize":100,
    "filters":[
        {
            "formQuestionSn":"c9d4455d1b88e72f",
            "filterType":12
        }
    ]
}
```


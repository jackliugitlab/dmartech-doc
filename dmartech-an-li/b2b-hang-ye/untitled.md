---
description: >-
  通过邮件或短信引导历史会员关注微信公众号，并完善会员渠道活跃标签，构建客户多维度标签体系，通过标签、来源、事件等指标完善用户画像，根据多场景下的客户行为数据，精准客户分层，提升运营效率。
---

# 多渠道数据打通场景

**视频讲解：**

[多渠道数据打通旅程视频教学](https://dmartech.oss-cn-shanghai.aliyuncs.com/help/B2B%E8%A1%8C%E4%B8%9A-%E5%9C%BA%E6%99%AF%E6%95%99%E7%A8%8B/2-%E5%A4%9A%E6%B8%A0%E9%81%93%E6%95%B0%E6%8D%AE%E6%89%93%E9%80%9A%E5%9C%BA%E6%99%AF/2-%E8%A7%86%E9%A2%91/1-2%E5%A4%9A%E6%B8%A0%E9%81%93%E6%89%93%E9%80%9A.mp4)

**图文教程：**

**1、流程总览**

![(邮件判断流程）](<../../.gitbook/assets/0 (5).png>)

![(手机短信判断流程）](<../../.gitbook/assets/1 (9).png>)

**2、解析步骤**

**步骤1：**新建人群标签，导入历史会员信息

![（联系人管理-联系人列表-上传数据）](<../../.gitbook/assets/2 (12).png>)

![(联系人管理-联系人列表-上传数据-选择标签）](<../../.gitbook/assets/3 (6).png>)

[点击查看联系人分群数据上传流程](https://doc.dmartech.cn/ke-hu-guan-li/ke-hu-guan-li-1)

![(联系人管理-联系人分群-新建分群）](<../../.gitbook/assets/4 (6).png>)

[点击查看联系人分群创建流程](https://doc.dmartech.cn/ke-hu-guan-li/ke-hu-fen-qun)

**步骤2：**新建邮件、短彩信素材

![(营销旅程-素材-邮件编辑）](<../../.gitbook/assets/5 (6).png>)

![（营销旅程-素材-短信/彩信）](<../../.gitbook/assets/6 (7).png>)

[点击查看营销旅程素材创建流程](https://doc.dmartech.cn/ying-xiao-lv-cheng/su-cai)

**步骤3：**搭建旅程

![(邮件判断流程）](<../../.gitbook/assets/8 (7).png>)

![(手机短信判断流程）](<../../.gitbook/assets/1 (9).png>)

[点击查看单次旅程创建流程](https://doc.dmartech.cn/ying-xiao-lv-cheng/ying-xiao-lv-cheng-1)

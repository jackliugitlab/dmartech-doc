---
description: >-
  通过邮件或短信激活沉睡会员，加强品牌认可度，提高会员互动；根据用户不同标签及所处阶段，自动化推送个性化内容，线索打分系统将根据客户互动行为不断更新，持续推动客户在不同生命周期的营销漏斗转化。通过设置线索智能分派流程，提高线索转化率。
---

# 沉睡会员激活场景

**视频讲解：**

[沉睡会员激活旅程视频教学](https://dmartech.oss-cn-shanghai.aliyuncs.com/help/B2B%E8%A1%8C%E4%B8%9A-%E5%9C%BA%E6%99%AF%E6%95%99%E7%A8%8B/3-%E6%B2%89%E7%9D%A1%E7%BA%BF%E7%B4%A2%E6%BF%80%E6%B4%BB%E5%9C%BA%E6%99%AF/2-%E8%A7%86%E9%A2%91/1-2%E6%B2%89%E7%9D%A1%E7%BA%BF%E7%B4%A2%E6%BF%80%E6%B4%BB.mp4)

**图文教程：**

**1、流程总览**

![](<../../.gitbook/assets/0 (6).png>)

**2、解析步骤**

**步骤1：**通过人群筛选功能，筛选出企业定义的沉睡会员人群

![(联系人管理-创建联系人分群）](<../../.gitbook/assets/1 (10).png>)

[点击查看联系人分群流程](https://doc.dmartech.cn/ke-hu-guan-li/ke-hu-fen-qun)

**步骤2：**新建邮件、短彩信素材

![(营销旅程-素材-新建邮件素材）](<../../.gitbook/assets/2 (13).png>)

![(营销旅程-素材-新建短信素材）](<../../.gitbook/assets/3 (7).png>)

[点击查看营销旅程素材创建流程](https://doc.dmartech.cn/ying-xiao-lv-cheng/su-cai)

**步骤3：**新建标签（根据自身企业情况新建会员的注册渠道、偏好等标签）

![(数据中心-标签管理-新建标签）](<../../.gitbook/assets/4 (7).png>)

[点击查看标签创建流程](https://doc.dmartech.cn/shu-ju-zhong-xin/shu-ju-guan-li/biao-qian-guan-li)

**步骤4：**搭建旅程

![(营销旅程-营销旅程-创建单次旅程）](<../../.gitbook/assets/5 (7).png>)

[点击查看单次旅程创建流程](https://doc.dmartech.cn/ying-xiao-lv-cheng/ying-xiao-lv-cheng-1)

**表单提交成功旅程：**

![（营销旅程-营销旅程-新建旅程）](<../../.gitbook/assets/6 (8).png>)

![(营销旅程-营销旅程-旅程编辑）](<../../.gitbook/assets/7 (9).png>)

[点击查看旅程编辑流程](https://doc.dmartech.cn/ying-xiao-lv-cheng/ying-xiao-lv-cheng-1)

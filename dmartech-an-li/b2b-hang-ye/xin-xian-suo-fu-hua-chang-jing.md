---
description: >-
  新注册会员进入生命周期、偏好标签划分，构建客户多维度标签体系，通过标签、来源、事件等指标完善用户画像，根据多场景下的客户行为数据，精准客户分层，提升运营效率。
---

# 新线索孵化场景

**视频讲解：**

[新线索孵化场景旅程视频教学](https://dmartech.oss-cn-shanghai.aliyuncs.com/help/B2B%E8%A1%8C%E4%B8%9A-%E5%9C%BA%E6%99%AF%E6%95%99%E7%A8%8B/1-%E6%96%B0%E7%BA%BF%E7%B4%A2%E5%AD%B5%E5%8C%96%E5%9C%BA%E6%99%AF/2-%E8%A7%86%E9%A2%91/1-2%E6%96%B0%E7%BA%BF%E7%B4%A2%E6%BF%80%E6%B4%BB.mp4)

**图文教程：**

**1、流程总览**

![](<../../.gitbook/assets/0 (4).png>)

**2、解析步骤**

**步骤1：**根据自身企业情况新建会员的注册渠道、偏好等标签

![(数据中心-标签管理-新建标签）](<../../.gitbook/assets/1 (8).png>)

[点击查看标签管理流程](https://doc.dmartech.cn/shu-ju-zhong-xin/shu-ju-guan-li/biao-qian-guan-li)

**步骤2：**新建表单（用户通过填写表单进行注册）

![(营销旅程-表单-新建表单）](<../../.gitbook/assets/2 (11).png>)

![(营销旅程-表单-编辑表单）](<../../.gitbook/assets/3 (5).png>)

![(营销旅程-表单-保存表单）](<../../.gitbook/assets/4 (5).png>)

![(营销旅程-表单-保存表单）](<../../.gitbook/assets/5 (5).png>)

![(营销旅程-表单-保存表单）](<../../.gitbook/assets/6 (6).png>)

[点击查看表单创建流程](https://doc.dmartech.cn/ying-xiao-lv-cheng/biao-dan)

**步骤3：**新建邮件素材

![(营销旅程-素材-新建邮件素材）](<../../.gitbook/assets/7 (7).png>)

[点击查看邮件素材创建流程](https://doc.dmartech.cn/ying-xiao-lv-cheng/su-cai)

**步骤4：**搭建旅程

![(营销旅程-营销旅程-创建实时旅程）](<../../.gitbook/assets/8 (6).png>)

![(营销旅程-营销旅程-编辑实时旅程）](<../../.gitbook/assets/9 (5).png>)

[点击查看实时旅程创建流程](https://doc.dmartech.cn/ying-xiao-lv-cheng/ying-xiao-lv-cheng-1)

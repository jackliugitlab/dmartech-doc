---
description: 介绍Dmartech通用的控件和筛选条件
---

# 基本概念

为了让您在阅读之后的内容，辅助您更好地理解，且能使用Dmartech的基本功能，本章我们为您介绍常用的控件和筛选条件的说明。

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

{% content-ref url="ming-ci-jie-shi.md" %}
[ming-ci-jie-shi.md](ming-ci-jie-shi.md)
{% endcontent-ref %}

{% content-ref url="kong-jian-shuo-ming.md" %}
[kong-jian-shuo-ming.md](kong-jian-shuo-ming.md)
{% endcontent-ref %}

{% content-ref url="shai-xuan-tiao-jian-shuo-ming.md" %}
[shai-xuan-tiao-jian-shuo-ming.md](shai-xuan-tiao-jian-shuo-ming.md)
{% endcontent-ref %}

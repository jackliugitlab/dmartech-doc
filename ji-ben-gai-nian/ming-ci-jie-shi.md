---
description: Dmartech的名词解释
---

# Dmartech涉及的专业名词解释

#### **公众号运营：**

只有绑定微信公众号的账号才可使用本模块。本模块的全部功能依托于微信官方开放接口，主要功能有粉丝管理、微信素材、微信消息发送、微信公众号菜单管理以及自动回复设置等，可与微信后台的相应内容实时同步。



**个性化二维码：**

Dmartech中创建并管理的二维码为带参二维码，为了满足用户渠道推广分析等场景的需要，可以创建带不同场景值的二维码，用户扫描后，公众号可以接收到事件推送。



**微信群发：**

给批量粉丝发送的图文消息、客服消息或模板消息。



#### 粉丝管理：

查看当前关注微信公众号的粉丝列表，可以添加备注和打粉丝标签。



#### 用户数据管理：

用户数据管理支持对联系人的增删改查。分为实名联系人、匿名联系人、微信粉丝联系人、全部联系人几类进行管理。



#### **用户：**

用户即联系人，平台中所有的用户数据均为联系人数据。



**实名联系人、匿名联系人、粉丝联系人、企微联系人、小程序联系人：**

实名联系人：手机号或者邮箱号有值的为实名联系人，即该联系人身份明确。

匿名联系人：手机号或者邮箱号均无值的为匿名联系人，比如网站的匿名访客。

粉丝联系人：关注公众号的粉丝自动同步为平台的联系人。

企微联系人：企微客户自动同步为平台的联系人。

小程序联系人：小程序用户自动同步为平台的联系人。



**联系人主键：**

用于作为联系人唯一身份标识的联系人属性，比如手机号、邮箱等。



**联系人属性：**

联系人属性又称为联系人字段属性，是Dmartech最基础的数据，通过这些数据可以有效的完善用户画像并分析和筛选数据。联系人属性主要是利用一些字段来完善联系人名片信息，比如姓名、手机号、邮箱、年龄、公司等，这些信息可以来自于联系人数据上传或创建，也可由客户提交表单，系统自动抓取联系人信息。



**联系人行为事件与事件属性：**

联系人的一些行为会被系统以行为事件的形式自动记录，比如邮件事件—打开邮件、点击邮件，当联系人有这些行为时，系统会自动记录，那么打开邮件名称、行为发生时间等则为事件属性，可用于描述和筛选事件。



**个性化标签、粉丝标签：**

个性化标签**：**用于标记联系人的特征。用于刻画联系人画像，以及分类和识别联系人。

粉丝标签：给微信粉丝添加的标签，用于分类和标记微信粉丝，其添加和管理都在微信粉丝管理界面，与微信公众后台同步。

****

#### 用户分群：

用户分群即联系人组的概念，通过联系人属性、行为事件来划分联系人群组，可用作于旅程中的受众。



#### 获客中心：

获客中心：获客中心用于收集联系人信息。在Dmartech创建好的表单，可以通过二维码或模板消息下发，联系人填写并提交表单后，数据会自动实时同步到Dmartech。

金数据表单：Dmartech对接了金数据表单，用户可以在金数据平台创建表单，在Dmrtech平台关联并同步，就可以直接使用金数据表单。

Dmartech表单：在Dmartech平台创建并管理的表单。



#### 营销素材：

平台允许创建并管理素材包括以下几种类型：邮件、短信、彩信、公众号素材、公众号模板消息。



#### 多渠道营销旅程：

营销就是通过手段让客户购买我们的服务或者产品。

营销旅程便是将营销方案逐步落实形成路径，用户进入旅程走完路径便完成了我们的营销方案。例如将大象放入冰箱内，需要三步：打开冰箱、放入大象、关门。同理，营销旅程并不复杂，只是将营销方案一步一步的搭建，设置，形成合理的流程就可以了。比如选择一批联系人，判断他们是否对运动鞋感兴趣，向他们发送运动鞋相关的产品介绍。



**营销渠道、任务：**

营销渠道：可触达用户的媒介方式即为渠道，比如邮件渠道、短信渠道、微信渠道。

任务：任意渠道的一次发送即为一个任务。



**里程碑：**

在旅程中设置里程碑可查看到达某个旅程环节的人数，用于分析营销效果。



#### 多渠道数据采集工具：

营销的基础是联系人数据。数据中心提供了多种数据接入方式，管理并记录数据导入记录和详细日志。

####

#### 营销旅程报告：

平台提供旅程概要报告与任务发送报告，同时平台也提供任务发送原始数据的导出。

发送总数：实际发送的总数。

送达总数：实际送达的总数。

独立打开：独立ID的打开最多计数一次，不按照打开次数累计。

独立点击：独立ID的点击最多计数一次，不按照点击次数累计。



{% content-ref url="kong-jian-shuo-ming.md" %}
[kong-jian-shuo-ming.md](kong-jian-shuo-ming.md)
{% endcontent-ref %}

{% content-ref url="shai-xuan-tiao-jian-shuo-ming.md" %}
[shai-xuan-tiao-jian-shuo-ming.md](shai-xuan-tiao-jian-shuo-ming.md)
{% endcontent-ref %}

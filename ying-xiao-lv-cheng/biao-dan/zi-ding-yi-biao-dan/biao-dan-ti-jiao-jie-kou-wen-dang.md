# 表单提交接口文档

### 1、Action提交方式

POST: /questionnaire/api/web-api/questionnaire/v1/submit/{formId}

_备注：{formId}可以通过导出的html中检索”paperId”_

Header：

&#x20;            Content-Type：application/x-www-form-urlencoded&#x20;

Request：&#x20;

&#x20;            __             PathParameters：&#x20;

&#x20;                        formIdInteger表单id&#x20;

Response：&#x20;

&#x20;            BodyParameters：&#x20;

&#x20;                       Key-Value表单提交数据，见示例

请求示例：

![](<../../../.gitbook/assets/image (591).png>)

返回码说明：

&#x20;           页面后台重定向自动跳转，无返回值。&#x20;

备注： 无

### 2、ajax提交方式

/questionnaire/api/web-api/questionnaire/v1/submitByForm/{formId}&#x20;

Header：&#x20;

&#x20;           Content-Type：multipart/form-data&#x20;

&#x20;           注意：不要使用RequestPayload方式&#x20;

Request：&#x20;

&#x20;           PathParameters：&#x20;

&#x20;                      formIdInteger表单id&#x20;

Response：&#x20;

Success200

![](<../../../.gitbook/assets/image (592).png>)

请求示例：

![](<../../../.gitbook/assets/image (593).png>)

![](<../../../.gitbook/assets/image (594).png>)

备注:&#x20;

&#x20;      设置隐藏字段【GOTOURL】时，type返回3，content会返回设置的跳转路径

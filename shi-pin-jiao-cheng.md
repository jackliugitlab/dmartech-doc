---
description: 分功能视频讲解教程，点击下方内容查看，括号内为视频长度。
---

# 视频教程

## 公众号运营

* [个性化二维码、微信群发](https://oss.dmartech.cn/help/%E4%BA%8C%E7%BB%B4%E7%A0%81%E5%88%97%E8%A1%A8\_%E5%8F%91%E9%80%81%E6%B6%88%E6%81%AF.mp4) (3:53)
* [自动回复、个性化菜单](https://oss.dmartech.cn/help/%E8%87%AA%E5%8A%A8%E5%9B%9E%E5%A4%8D\_%E8%87%AA%E5%AE%9A%E4%B9%89%E8%8F%9C%E5%8D%95.mp4) (2:24)
* [留言回复、粉丝管理](https://oss.dmartech.cn/help/%E6%B6%88%E6%81%AF%E7%AE%A1%E7%90%86%E5%8F%8A%E7%B2%89%E4%B8%9D%E7%AE%A1%E7%90%86.mp4)(2:32)
* [素材管理、模板消息](https://oss.dmartech.cn/help/%E7%B4%A0%E6%9D%90%E6%B6%88%E6%81%AF%E5%8F%8A%E6%A8%A1%E6%9D%BF%E6%B6%88%E6%81%AF.mp4)(2:25)

## 用户管理

* [用户画像](https://oss.dmartech.cn/help/%E8%81%94%E7%B3%BB%E4%BA%BA%E7%AE%A1%E7%90%86.mp4)(3:13)
* [用户分群](https://oss.dmartech.cn/help/%E8%81%94%E7%B3%BB%E4%BA%BA%E5%88%86%E7%BE%A4.mp4)(3:45)

## 智慧营销管理

* [个性化邮件、个性化短信、个性化彩信](https://oss.dmartech.cn/help/%E5%88%9B%E5%BB%BA%E7%B4%A0%E6%9D%90.mp4)(6:40)
* [多渠道营销旅程](https://oss.dmartech.cn/help/%E8%90%A5%E9%94%80%E6%97%85%E7%A8%8B.mp4)(6:25)
* [获客中心](https://oss.dmartech.cn/help/%E8%A1%A8%E5%8D%95.mp4) （5:29）

## CPM同意与偏好管理

* [多渠道数据采集工具、用户元数据配置管理](https://oss.dmartech.cn/help/%E6%95%B0%E6%8D%AE%E4%B8%AD%E5%BF%83\_%E5%85%83%E6%95%B0%E6%8D%AE.mp4)(2:46)
